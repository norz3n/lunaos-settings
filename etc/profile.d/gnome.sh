# Fix cursor bug in GNOME XORG
if [[ $DESKTOP_SESSION == "gnome-xorg" ]]; then
  xset -b off
fi

# QT
if [[ $DESKTOP_SESSION == "gnome" || $DESKTOP_SESSION == "gnome-xorg" ]]; then
	export QT_AUTO_SCREEN_SCALE_FACTOR=1
	export QT_STYLE_OVERRIDE=kvantum
fi
