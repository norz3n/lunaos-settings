#!/bin/bash

if [ $(command -v bluetoothctl) ]; then
	bluetoothctl power off
	systemctl stop bluetooth
	rfkill block bluetooth
	rfkill unblock bluetooth
	systemctl start bluetooth
	sleep 1
	bluetoothctl power off
fi
