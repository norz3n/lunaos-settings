if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
	if [ -d /sys/module/nouveau ]; then
		export QSG_RENDER_LOOP='basic'
	fi
	# Workaround for Alacritty title bar
    export WINIT_UNIX_BACKEND=x11
    export MOZ_ENABLE_WAYLAND=1
fi
