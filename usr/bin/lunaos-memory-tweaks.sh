#!/usr/bin/env bash

ram_size=$(free -g | grep Mem | awk '{print $2}')

# Iterate over the block devices and check if they are zram devices
for device in /sys/block/zram*; do
    if [[ -d "$device" ]]; then
      sysctl -w vm.page-cluster=0
    fi
done

# Check if a swap file or partition exists
if [[ $(swapon --show) ]]; then
  # A swap file or partition exists, set vm.swappiness to 100
  sysctl -w vm.swappiness=100
fi

# Check if SSD, HDD, or NVMe
if lsblk -d -o rota | grep -q '^0$'; then
  storage_type="ssd"
elif lsblk -d -o tran | grep -q 'nvme'; then
  storage_type="nvme"
else
  storage_type="hdd"
fi

# Set vm.vfs_cache_pressure based on storage type
if [[ $storage_type == "ssd" ||  $storage_type == "nvme" ]]; then
  sysctl -w vm.vfs_cache_pressure=50
elif [[ $ram_size -gt 1 && $storage_type == "hdd" ]]; then
  sysctl -w vm.vfs_cache_pressure=1000
fi
